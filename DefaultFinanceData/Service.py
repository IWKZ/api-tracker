import requests
import datetime
import json
import logging
import config

class Service:
  def __init__(self):
    self.finance_type = ""
    self.categories = ("")
    self.type_key = {"income": "totalIncomeByTypes", "bill": "totalBillByTypes"}
    self.type_id_key = {"income": "incomeTypeId", "bill": "billTypeId"}

  def set_finance_type(self, finance_type):
    self.finance_type = finance_type
    return self

  def set_categories(self, categories):
    self.categories = categories
    return self

  def get_data_previous_month(self):
    if self.finance_type == "" or len(self.categories) == 0:
      logging.error("finance type or categories not yet set")
      return

    filtered_categories = self._remove_existing_categories()

    if len(filtered_categories) == 0:
      logging.info("categories already added!")
      return

    date = self._get_date(True)
    date_params = "?month=" + date["month"] + "&year=" + date["year"]
    data = self._get_data(date_params)

    return {k: data[k] for k in data if k in filtered_categories}

  def add_new_data(self, new_data):
    if new_data is None or len(new_data) == 0:
      logging.warning("no new data found!")
      quit()

    categories = new_data.keys()
    url = config.API_URL + "/api/" + self.finance_type
    date = self._get_date()
    prev_date = self._get_date(True)
    info = "total from previous month " + ".".join(prev_date.values())
    headers = {"Authorization": "Bearer " + config.API_TOKEN, "Content-Type": "application/json"}
    category_ids = config.DEFAULT_STATIC_INCOME if (self.finance_type == "income") else config.DEFAULT_STATIC_BILL

    for category in categories:
      data = {"month": date["month"], "year": date["year"], "info": info}
      data[self.type_id_key[self.finance_type]] = category_ids[category]
      data["amount"] = new_data[category]

      logging.info("add data from previous month " + json.dumps(data))

      requests.post(url, headers=headers, json=data)

  def _remove_existing_categories(self):
    date = self._get_date()
    date_params = "?month=" + date["month"] + "&year=" + date["year"]
    data = self._get_data(date_params)

    existing_categories = data.keys()

    return [cat for cat in self.categories if cat not in existing_categories]

  def _get_data(self, date_params):
    types_key = self.type_key[self.finance_type]

    url = config.API_URL + "/api/" + self.finance_type + "/total" + date_params

    response = requests.get(url)
    return response.json()["data"][0][types_key]
  
  def _get_date(self, is_prev_date = False):
    today = datetime.date.today()
    first_day = today.replace(day=1)
    prev_date = first_day - datetime.timedelta(days=1)

    month = prev_date.month if is_prev_date else today.month
    year = prev_date.year if is_prev_date else today.year

    return {"month": str(month), "year": str(year)}

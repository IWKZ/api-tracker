import sys
import config
import logging
from Service import Service

# setup log
format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(filename=config.LOG_FILE, filemode='a', format=format, level=logging.INFO)

# main tracker
service = Service()
finance_types = ("income", "bill")
for finance_type in finance_types:
  categories = config.DEFAULT_STATIC_INCOME if finance_type == "income" else config.DEFAULT_STATIC_BILL

  service.set_finance_type(finance_type).set_categories(categories.keys())
  
  data = service.get_data_previous_month()
  service.add_new_data(data)
